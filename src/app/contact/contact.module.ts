import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactComponent } from './contact.component';
import{ProfileformComponent}from'./profileform/profileform.component';
import{FormsModule}from'@angular/forms';
import { ContmodifiComponent } from './contmodifi/contmodifi.component';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import { ContactService } from "./contact.service";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BrowserModule,
    HttpClientModule

  ],
  exports:[
    ContactComponent
  ],
  providers:[
    ContactService
  ],


  declarations: [ContactComponent]
})
export class ContactModule { }
