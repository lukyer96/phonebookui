import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileformComponent } from './profileform.component';
import { ContactService } from "../contact.service";
@NgModule({
  imports: [
    CommonModule
  ],
  exports:[
    ProfileformComponent
  ],
  providers:[
    ContactService
  ],
  declarations: [ProfileformComponent]
})
export class ProfileformModule { }
