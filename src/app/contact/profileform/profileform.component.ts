import { Component, OnInit,Input,Output, EventEmitter } from '@angular/core';
import { ContactService } from "../contact.service";

@Component({
  selector: 'app-profileform',
  templateUrl: './profileform.component.html',
})
export class ProfileformComponent implements OnInit {
  profileform=[]
  @Output() onForm: EventEmitter<any> = new EventEmitter();
  Form(){
    this.onForm.emit()
  }


  constructor(private ContactService: ContactService) { }

  ngOnInit() {
  
    this.ContactService.featchContact().subscribe(data =>{ this.profileform=data});
  }

}
