import { Injectable } from '@angular/core';
import { Http,RequestOptions } from "@angular/http";
@Injectable()
export class ContactService {

  constructor(private htpp:Http) { }
  featchContact(){
    return this.htpp.get("https://phonebook-back.herokuapp.com/user").map(res=> res.json())
    
  }
  addPerson(values){
    console.log(JSON.stringify(values))
    return this.htpp.put("https://phonebook-back.herokuapp.com/user/",values).map(res=> res)
  }
  deletePerson(values){
    console.log(JSON.stringify(values))
    return this.htpp.delete("https://phonebook-back.herokuapp.com/user/",new RequestOptions({body: values,})).map(res=> res)
  }
}
