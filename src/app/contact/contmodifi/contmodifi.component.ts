import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { ContactService } from "../contact.service";


@Component({
  selector: 'contmodifi',
  templateUrl: './contmodifi.component.html',
  
})
export class ContmodifiComponent implements OnInit {
message
  @Output() onList: EventEmitter<any> = new EventEmitter();
  constructor(private ContactService: ContactService) { }

  ngOnInit() {
  }
  List(){
    this.onList.emit()
  }
  Add(values){
    this.ContactService.addPerson(values).subscribe(data =>{ this.message=data.text()});
    
  }
  Delete(values){
   
    this.ContactService.deletePerson(values).subscribe(data =>{ this.message=data.text()});
  }
  
}
