import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContmodifiComponent } from './contmodifi.component';

describe('ContmodifiComponent', () => {
  let component: ContmodifiComponent;
  let fixture: ComponentFixture<ContmodifiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContmodifiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContmodifiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
