import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContmodifiComponent } from './contmodifi.component';
import{FormsModule}from'@angular/forms';
import { ContactService } from "../contact.service";
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
  ],
  exports:[
    ContmodifiComponent
  ],
  providers:[
    ContactService
  ],
  declarations: [ContmodifiComponent]
})
export class FormModule { }
